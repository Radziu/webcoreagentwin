﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using Microsoft.Win32;

namespace WCAgentWin
{
    static class InventorySprzet
    {
        public static string SerialNumber()
        {
            string serial = string.Empty;
            ManagementObjectSearcher MOS = new ManagementObjectSearcher("Select * From Win32_BaseBoard");
            foreach (ManagementObject getserial in MOS.Get())
            {
                serial = getserial["SerialNumber"].ToString();
            }
            return serial;
        }
        public static string LoggedUser()
        {
            return Environment.UserName;
        }
        public static string OsVersion()
        {
            var reg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion");
            string productName = (string)reg.GetValue("ProductName");
            return productName;
        }
        //Do uzupelnienia kiedy bedzie wymyslone jak tworzyc baze przy instalacji aplikacji
        //public static DateTime FirsLogin()
        //{

        //}
        public static string OsBuild()
        {
            var b = Environment.OSVersion.Version.Build;
            return b.ToString();
        }
        public static string ComputerName()
        {
            return Environment.MachineName;
        }
    }
}
